import React from 'react';
import PropTypes from 'prop-types';

/**
 * FormButton.
 * Shows the  submit button form with state 
 * Eg. loading === true ? is disabled button : is enabled button.
 * @author [Junior Campos](junior.uc.9192@gmail.com)
 */
const FormButton = (
	{
		text, 
		textLoading, 
		className, 
		isLoading
	}
) => {
  return (
    <button
    		type="submit"
        className={className}
        disabled={isLoading}>
        {
        	isLoading ? (
        		<>{textLoading}</>
        	) : (
        		<>{text}</>
        	)
        }
    </button>
  )
}

FormButton.defaultProps = {
	text: "Login",
	textLoading: "Sending...",
	className: "btn btn-primary",
	isLoading: false
};

FormButton.propTypes = {
	text: PropTypes.string,
	textLoading: PropTypes.string,
  	className: PropTypes.string,
  	isLoading: PropTypes.bool,
};

export default FormButton;
