import React from 'react';
import { Form } from 'react-final-form';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { Redirect } from 'react-router-dom';
import { validate } from 'utils/forms/validators';
import { InputField } from 'utils/forms/renderings';
import FormButton from 'components/common/FormButton';
import schema from 'components/Login/schema';
import iconFire from 'assets/noun-fire-2475241.svg';
import iconShape from 'assets/shape.svg';
import { STEP_ONE, STEP_TWO } from 'components/Login/constants';
import './styles/Login.scss';

class Login extends React.Component {
    state = {
        step: STEP_ONE,
        loading: false,
        authenticationError: false,
        isAuthenticated: false
    }

    /**
     * Change the next step
     * @return {Promise}
    */
    nextStep = () => 
        this.setState({ step: STEP_TWO });

    /**
     * Handle submit form
     * @param  {Object} user 
     * @return {Promise}
    */
    handleSubmit = user => {
        this.setState({loading: true});
        // Simulate Network Latency
        setTimeout(() => {
            if ( user.login === 'linktic' && user.password === '12345678')
                this.loginWithError(user);
            else
                this.loginWithoutError(user);
        }, 2000);
    }

    /**
     * Fake the login request with error
     * @param  {Object} user 
    */
    loginWithError = user => {
        const mock = new MockAdapter(axios);
        mock.onPost('/login').reply(403, {
            error: { 
                "Auth":"Denied"
            }
        });
        axios.post('/login', user)
            .catch(err => {
                console.log("Log Error: ", err.response.data.error);
                this.setState({ 
                    loading: false,
                    authenticationError: true 
                });
            });
    }

    /**
     * Fake the login request without error
     * @param  {Object} user 
    */
    loginWithoutError = user => {
        const mock = new MockAdapter(axios);
        mock.onPost('/login').reply(200, {
            key: {
                "Auth": "Logged",
                "Language": "EN"
            }
        });
        axios.post('/login', user)
            .then(response => {
                console.log("Server response: ", response.data);
                this.setState({ 
                    loading: false,
                    authenticationError: false 
                }, () => {
                    this.nextStep();
                });
            });
    }

    /**
     * User mark as authenticated
    */
    successLogin = () => {
        this.setState({ isAuthenticated: true });
    }

    render() {
        const { 
            step, 
            loading, 
            authenticationError,
            isAuthenticated
        } = this.state;

        if ( isAuthenticated )
            return <Redirect to="/success-login" />
        
        return (
            <div className="container d-flex flex-column justify-content-center h-100">
                <Form
                onSubmit={this.handleSubmit}
                validate={values => validate(values, schema())}>
                    {({handleSubmit, values, submitting }) => (
                        <div className="row justify-content-center">
                            <form
                                onSubmit={handleSubmit} 
                                className="login-form col-sm-12 col-md-6 col-lg-4">
                                <div className="d-flex mb-4">
                                    <img 
                                        src={iconFire} 
                                        alt="icon fire"
                                        className="icon-fire" />
                                    <h3 className="ml-3 mt-auto mb-0">Login</h3>
                                </div>
                                {
                                    step === STEP_ONE ? (
                                        <>
                                            <div className="form-group mb-4">
                                                <InputField
                                                    name="login"
                                                    placeholder="Login"
                                                    className={authenticationError ? 'form-control form-control-lg is-invalid' : undefined} />
                                            </div>
                                            <div className="form-group mb-4">
                                                <InputField
                                                    type="password"
                                                    name="password"
                                                    placeholder="Password" />
                                            </div>
                                            <FormButton
                                                    className="d-flex btn btn-primary ml-auto px-4"
                                                    isLoading={loading} />
                                            <p className="is-simply-dummy-text mt-5 text-center">
                                                is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                                            </p>
                                            <button className="btn-custom btn-rounded px-5 d-flex m-auto">
                                                <img
                                                        width="27px"
                                                        height="27px" 
                                                        src={iconShape} 
                                                        alt="icon shape"
                                                        className="icon-shape" />
                                            </button>
                                            
                                        </>
                                    ) : (
                                        <>
                                            <p className="is-simply-dummy-text mt-4 text-center">
                                                Two seep autentication required for this login for this user login
                                            </p>
                                            <div className="form-group mb-3">
                                                <InputField
                                                    name="authCode"
                                                    placeholder="Enter two step auth code" />
                                            </div>
                                            <button
                                                onClick={this.successLogin}
                                                type="button" 
                                                className="btn-custom px-5 text-primary d-flex m-auto">
                                                Continue
                                            </button>
                                        </>
                                    )
                                }
                            </form>
                        </div>
                    )}
                </Form>
            </div>
        );
    }
}

export default Login;