import * as yup from 'yup';
import { REQUIRED_FIELD, MIN_8 } from 'constants/index';

/**
 * Schema for validation login form
 * @return {Object} validation object
 */
const schema = () =>
  yup.object().shape({
    login: yup
      .string()
      .max(255)
      .required(REQUIRED_FIELD),
    password: yup
      .string()
      .min(8, MIN_8)
      .required(REQUIRED_FIELD),
    authCode: yup
      .string()
      .min(8, MIN_8)
  });

export default schema;