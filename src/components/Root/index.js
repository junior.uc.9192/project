import React from 'react';
import { 
    BrowserRouter as Router, 
    Switch, 
    Route 
} from 'react-router-dom';
import Login from 'components/Login';
import Success from 'components/Success';

/**
 * Managements of router
 */
const Root = () =>
    <Router>
        <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/success-login" component={Success} />
        </Switch>
    </Router>

export default Root;
