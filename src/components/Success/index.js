import React from 'react';
import iconSuccess from 'assets/noun-success-2484180.svg';

const Success = () => (
    <div className="container d-flex flex-column justify-content-center h-100">
        <div className="d-flex justify-content-center align-items-center">
            <img 
                src={iconSuccess} 
                alt="icon success"
                className="icon-success" />
            <h3 className="ml-3 mb-0">Succesful Loged</h3>
        </div>
    </div>
)

export default Success;