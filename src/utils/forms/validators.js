/** 
 *  @fileOverview Utilities for handling forms validations.
 *
 *  @author [Junior Campos] (junior.uc.9192@gmail.com)
 */
import React from 'react';
import { Field } from 'react-final-form';
import { setIn } from 'final-form';

/**
 * Show the field errors
 * @param  {String} name Field name
 * @return {Field}
 */
export const FieldError = ({ name }) => (
  <Field
    name={name}
    subscribe={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) =>
      touched && error ? <small className="text-italic text-danger">{error}</small> : null
    } />
);

/**
 * Validate values of form
 * @param  {Object} values Values form
 * @param  {Object} schema Validation schema
 * @return {Object}
 */
export const validate = async (values, schema) => {
  if (typeof schema === 'function')
    schema = schema();

  try {
    await schema.validate(values, { abortEarly: false });
  } catch (e) {
    return e.inner.reduce((errors, error) => {
      return setIn(errors, error.path, error.message);
    }, {});
  }
};