/** 
 *  @fileOverview Components for handling fields forms.
 *  eg. input => type[text, password], select
 *
 *  @author [Junior Campos] (junior.uc.9192@gmail.com)
 */
import React from 'react';
import { Field } from 'react-final-form';
import { FieldError } from 'utils/forms/validators';

/**
 * Render the input tag with label & error field
 * @param  {String} options.name        Field name 
 * @param  {String} options.labelText   Field label
 * @param  {String} options.placeholder Field placeholder
 * @param  {String} options.validate    Field validate
 * @param  {String} options.type        Field type (eg. [text, password])
 * @param  {String} options.className   Field class
 * @param  {Boolean} options.autoFocus  Field focus
 * @return {Node}                     
 */
export const InputField = ({
  name,
  labelText,
  placeholder,
  validate,
  type,
  className,
  autoFocus
}) => (
  <>
    <Field
      name={name}
      component="input"
      type={type}
      className={className}
      placeholder={placeholder}
      validate={validate}
      autoFocus={autoFocus} />
    <FieldError name={name} />
  </>
);

InputField.defaultProps = {
  type: 'text',
  className: 'form-control form-control-lg',
  autoFocus: false
};
