// FORMS
export const REQUIRED_FIELD = 'This field is required';
export const MIN_8 = 'This field must be at least 8 characters';